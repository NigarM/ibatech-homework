
function createNewUser() {
    const newUser={
        firstName: prompt("enter your first name"),
        lastName:prompt("enter your last name"),
        birthDay: prompt("enter your birth date", "dd.mm.yyyy"),
        getLogin:function(){
            return (this.firstName.charAt(0)+this.lastName).toLowerCase()
        },
        getAge: function(){
            let strBirth = this.birthDay.split('.');
            let userDay = strBirth[0];
            let userMonth = strBirth[1];
            let userYear = strBirth[2];
            let currentDate= new Date();
            let currentDay = currentDate.getDate();
            let currentMonth = currentDate.getMonth()+1;
            let currentYear = currentDate.getFullYear();

            let age = currentYear - userYear;

            if (currentMonth > userMonth) {
                return age;
            } else {
                if (currentDay >= userDay) {
                    return age;
                } else {
                    age--;
                }
            }
            return age;
        },

        getPassword:function () {
            return (this.firstName[0].toUpperCase()+this.lastName.toLowerCase()+this.birthDay.slice(-4))
        }

    };
    return newUser;
}
let user = createNewUser();
console.log(user.getAge());
console.log(user.getPassword());
